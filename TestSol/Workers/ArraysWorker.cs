using System;

namespace TestSol.Workers
{
    public class ArraysWorker
    {
        public int[] InnerArray { get; set; }

        private int maxValue;
        public int MaxValue
        {
            get => maxValue;
            set
            {
                if (value > 0)
                {
                    maxValue = value;
                }
            }
        }
        public ArraysWorker() : this(100)
        {}

        public ArraysWorker(int number)
        {
            MaxValue = int.MaxValue;
            InnerArray = new int[number];
        }

        public void FillingWithRandom()
        {
            var random = new Random();
            for (int i = 0; i < InnerArray.Length; i++)
            {
                InnerArray[i] = random.Next(maxValue);
            }
        }

        public void Display()
        {
            for (int i = 0; i < InnerArray.Length; i++)
            {
                Console.WriteLine("Array[{0}] = {1}", i, InnerArray[i]);
            }
        }

        public override string ToString()
        {
            string result = "";
            for (int i = 0; i < InnerArray.Length; i++)
            {
                result += InnerArray[i] + " ";
            }

            return result;
        }
    }
}
