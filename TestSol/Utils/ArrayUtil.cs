using System.Threading.Tasks;

//Hello world
namespace TestSol.Utils
{
    public class ArrayUtil
    {
        public static int Max(int[] array)
        {
            int max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (max < array[i])
                    max = array[i];
            }

            return max;
        }

        public static async Task<int> MaxAsync(int[] array)
        {    
            return await Task.Run(() => Max(array));
        }
        
        //Min
        
        //Average
    }
}