﻿using System;
using System.Threading.Tasks;
using TestSol.Workers;
using TestSol.Utils;

namespace TestSol
{
    internal class MainClass
    {   
        public static async Task Main(string[] args)
        {
            ArraysWorker arraysWorker = new ArraysWorker();
            arraysWorker.MaxValue = 100;
            arraysWorker.FillingWithRandom();
            arraysWorker.Display();
       
            int max = ArrayUtil.Max(arraysWorker.InnerArray);
            
            Console.WriteLine("Without async method = " + max);
            
            int max2 = await ArrayUtil.MaxAsync(arraysWorker.InnerArray);
            
            Console.WriteLine("With async method = " + max2);    
        }
    }
}